---
title: Configuration de vim
author: Germain
type: post
date: 2012-04-02
tags:
  - vim
  - configuration
---

Voici le contenu de mon fichier `.vimrc`. Cette configuration n&rsquo;est pas la meilleure puisqu&rsquo;elle est adaptée à ma façon de faire mais vous trouverez peut être des exemples pour construire votre propre configuration.

La fin du fichier concerne l&rsquo;utilisation de [vim-latex][1]. Il s&rsquo;agit d&rsquo;un plugin pour vi ajoutant des fonctionnalités intéressantes pour l&rsquo;écriture de fichiers latex avec vi. Ce plugin est disponible dans les dépôts ubuntu officiels ou directement sur le site du plugin.

```vimrc
--------------------------------------------------------------------
"  abandonne les regles de vim anciennes versions
" --------------------------------------------------------------------
set nocompatible
set backspace=2	" pour un fonctionnement correct du backspace

" --------------------------------------------------------------------
"  COULEURS:
" --------------------------------------------------------------------
colorscheme default
highlight IncSearch term=underline ctermbg=LightGreen ctermfg=NONE guibg=LightGreen guifg=NONE
highlight Search term=underline ctermbg=LightGreen ctermfg=NONE guibg=LightGreen guifg=NONE
highlight Cursor guifg=white guibg=grey30
highlight CursorLine term=underline cterm=underline guibg=#F4F4F4
highlight CursorColumn term=NONE ctermbg=grey guibg=#F4F4F4
highlight StatusLine term=reverse,bold cterm=reverse,bold gui=italic guibg=SteelBlue guifg=white
highlight StatusLineNC term=reverse cterm=reverse gui=italic guibg=grey75 guifg=SteelBlue
highlight Error gui=bold guifg=red guibg=yellow

" --------------------------------------------------------------------
"  RECHERCHE:
" --------------------------------------------------------------------
set hlsearch	" active surbrillance
set incsearch	" se deplace pendant la frappe
set ignorecase	" ignore la casse

" --------------------------------------------------------------------
"  STATUS LINE:
" --------------------------------------------------------------------
set ruler          " Affiche le numero de la ligne/colonne
set showmode       " Affiche le mode courant
set showcmd        " Affiche des commandes
set laststatus=2   " affiche la status line
set statusline=fichier:\ %m\%F\ %y\ %=\ ligne\ %l\ colonne\ %c\ --\%P\--

" --------------------------------------------------------------------
"  APPARENCE:
" --------------------------------------------------------------------
syntax on                    " coloration syntaxique
let fortran_free_source=1    " fortran syntax is free format (f90)

set visualbell t_vb=	" pas de message sonore ni visuel
			" When the GUI starts, 't_vb' is reset to its default value.
			" You have to set it again in your gvimrc
set nowrap		" pas de retour à la ligne
set textwidth=90	" coupe la ligne si elle depasse 90 caractères
set showmatch		" affiche les paires de parenthese
set cursorline		" highlight current line
set autoindent		" copy indent from the current line to the new line
set smartindent		" automatically indent a line depending on previous line
set expandtab           " remplace les tab par des espaces
set shiftwidth=4        " indentation = 4 espaces
set encoding=utf-8
" set guifont=Mono\ 9

" --------------------------------------------------------------------
"  SOURIS:
" --------------------------------------------------------------------
set mouse=a             " active le deplacement a la souris
                        " peut causer des problemes pour copier/coller

" --------------------------------------------------------------------
"  ABBREVIATIONS ET RACCOURCIS CLAVIER:
" --------------------------------------------------------------------
ab ccom /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
ab fcom ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
ab lcom % * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
ab pcom # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

" mise a zero du champ de recherche avec F2 : supprime la surbriallance apres la recherche
nnoremap &lt;silent> &lt;F2> :silent noh&lt;cr>

" nouvel onglet
map &lt;C-n> &lt;Esc>:tabnew&lt;cr>
" navigateur de fichier
map &lt;C-e> &lt;Esc>:Exp&lt;cr>
" sauvegarde
map &lt;C-s> &lt;Esc>:w&lt;cr>
" tout selectionner
map &lt;c-a> ggVG

" --------------------------------------------------------------------
"  ORTHOGRAPHE:
" --------------------------------------------------------------------
" set spell
" set spelllang=fr
map &lt;F3> :setlocal spell spelllang=fr&lt;cr>
map &lt;F4> :set nospell&lt;cr>

" --------------------------------------------------------------------
"  PLUGINS:
" --------------------------------------------------------------------
" charge le bon plugin à l'ouverture du fichier
filetype on
filetype plugin on

" --------------------------------------------------------------------
"  VIMLATEX:
" --------------------------------------------------------------------
" PATH: ajout dans le path pour chercher vim-latex-suite
" set runtimepath+=/usr/share/vim/addons/

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
" set shellslash

" IMPORTANT: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file-name.
set grepprg=grep\ -nH\ $*

" OPTIONAL: This enables automatic indentation as you type.
"filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" TIP: if you write your \label's as \label{fig:something}, then if you
" type in \ref{fig: and press &lt;C-n> you will automatically cycle through
" all the figure labels. Very useful!
autocmd FileType *.tex set iskeyword+=:
" => conflit avec else: de python

" REPLIS: vim-latex replis automatiquement certaines sections et environnements
" ou commandes. La liste de ce qui doit être replié est géré par les varibales
" globales suivantes. Les replis se font en partant de la fin de la liste puis
" en remontant. Les defaut sont dans folding.vim (~/.vim/ftplugin/latex-suite/)
let g:Tex_FoldedSections="part,chapter,section,%%fakesection,subsection"
let g:Tex_FoldedEnvironments="verbatim,comment,eq,align,figure,table,tabular,tikzpicture,thebibliography,abstract,frame"
let g:Tex_FoldedMisc = 'preamble,&lt;&lt;&lt;'

" COMPILATION VISUALISATION: par defaut on compile avec pdflatex et on utilise
" pour visualiser les pdf : evince
let g:Tex_DefaultTargetFormat="pdf"
let g:Tex_ViewRule_pdf = "evince"
let g:Tex_CompileRule_pdf="pdflatex -shell-escape -interaction=nonstopmode $*"
```

J'utilise les plugins :

  * [vimlatex][1] : pour latex
  * pyflakes : pour python [(voir ici)][2]

 [1]: http://vim-latex.sourceforge.net/
 [2]: https://gsalvatovallverdu.gitlab.io/post/2012-04-02-python-et-gvim/
