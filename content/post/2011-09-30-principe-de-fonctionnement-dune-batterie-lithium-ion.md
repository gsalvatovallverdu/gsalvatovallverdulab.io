---
title: Principe de fonctionnement d'une batterie lithium ion
author: Germain
type: post
date: 2011-09-30
featured_image: /img/batt_decharge.png
toc: false
tags:
  - sciences
  - physical-chemistry
  - teaching
---

Le document ci-dessous présente, de façon simple, le fonctionnement d'une batterie lithium
ion. Il s'agit de diapositives que j'ai utilisées à l'occasion
d'un séminaire à destination  d'élèves du lycée. La première partie est un
bref historique au sujet des batteries. La deuxième partie concerne quelques rappels en
électricité et sur les piles ou batteries en général. La troisième partie traite des
batteries lithium ion.


[Présentation sur les batterie Lithium-ion (pdf, 1.1 Mo)](/docs/BatterieLiion.pdf)


#### Shémas de fonctionnement

<img src="/img/batt_encharge.png" style="display:inline-block; width: 48%;"/>
<img src="/img/batt_decharge.png" style="display:inline-block; width: 48%;"/>

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence
Creative Commons" style="border-width:0;" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

Les images ci-dessus sont disponibles au format png (300 dpi) ou pdf. Elles
ont été faites avec [tikz](http://www.texample.net/tikz/examples/) et mises
à disposition selon les termes de la licence [Creative Commons Attribution CC - BY -
SA](http://creativecommons.org/licenses/by-sa/4.0/)

* [Batterie en charge (png, 300dpi, 233Ko)](/img/batt_encharge.png)
* [Batterie en fonctionnementr (png, 300dpi, 233Ko)](/img/batt_decharge.png)
* [Batterie en charge (pdf, 29Ko)](/img/batt_encharge.pdf)
* [Batterie en fonctionnementr (pdf, 33Ko)](/img/batt_decharge.pdf)

#### Sources

Voici deux documents grand public sur le sujet :

  * Science et vie 1076, mai 2007
  * La recherche 435, Novembre 2009
