---
title: "Outils pour la symétrie moléculaire : théorie des groupes"
author: Germain
type: post
date: 2012-06-09
toc: false
tags:
    - python
    - physical-chemistry
    - teaching
---

En chimie-physique, on utilise les propriétés de symétrie du système pour simplifier
la résolution de certaines équations. L'outil mathématiques à la base de l'utilisation de la symétrie en chimie est la théorie des groupes. Le livre &laquo;&nbsp;chimie et théorie des groupes&nbsp;&raquo; de Paul H. Walton (édition De Boeck) est un ouvrages bien adapté aux chimistes ou physico-chimistes qui désirent découvrir cette approche.

Lorsqu&rsquo;on utilise la théorie des groupes, une étape primordiale et très mécanique est la décomposition de la représentation d&rsquo;un groupe en représentation irréductibles de ce groupe (analogue à déterminer les coordonnées d&rsquo;un vecteur dans une base orthonormée). Le module python ci-dessous permet d&rsquo;obtenir automatiquement la décomposition. Voici un exemple :

```python
>>>> from groupes import c4v
>>> rep = [4, 0, 0, 2, 0]
>>> print(c4v)
 C4v  |     1E      2C4      1C2    2sigma_v 2sigma_d
------------------------------------------------------------
  A1  |     1        1        1        1        1
  A2  |     1        1        1        -1       -1
  B1  |     1        -1       1        1        -1
  B2  |     1        -1       1        -1       1
  E   |     2        0        -2       0        0
>>> c4v.ordre
8
>>> c4v.reduce(rep)
1 A1 + 1 B1 + 1 E
```

Je n&rsquo;ai pas entré tous les groupes ponctuels de symétrie mais uniquement ceux dont j&rsquo;ai eu besoin pour le moment. Les groupes disponibles sont :

  * C2v
  * C3v
  * C4v
  * D3h
  * D4h

Le module python est disponible sur ici [groupes.py](https://github.com/gVallverdu/myScripts/blob/master/Various/groupes.py)
