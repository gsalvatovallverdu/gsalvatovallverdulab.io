---
title: Photoelectric effect
author: Germain
type: post
date: 2017-01-09
toc: false
tags:
  - sciences
  - gif
---

Here are provided two simple gif images in order to illustrate the
[photoelectric effect](https://en.wikipedia.org/wiki/Photoelectric_effect).

Below the threshold energy, nothing append, whatever the light intensity.

![No photoelectric effect](/img/belowth.gif)
_No photoelectric effect_

Above the threshold energy, each photon, bear enough energy, a quantum of energy,
in order to extract electrons from the material.

![Photoelectric effect](/img/aboveth.gif)
_Photoelectric effect_

All pictures can be downloaded here : [photoelec.zip](/zip/photoelec.zip)
