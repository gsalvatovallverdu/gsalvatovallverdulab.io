---
title: Python et (g)vim
author: Germain
type: post
date: 2012-04-02
tags:
  - python
  - vim
---

Je donne ici une configuration et des plugins adaptés à l&rsquo;écriture de code python avec vi. J&rsquo;utilise la version graphique de vi : gvim. Ce que je propose ici n&rsquo;est donc peut être pas commode en pratique avec la version terminal de vi.

### plugins

Voici les plugins vi que j&rsquo;utilise :

  * [comments.vim][1] : permet de commenter ou décommenter une ligne ou un groupes de lignes avec les touches ctrl+C ctrl+X. Ce plugin fonctionne avec tout une liste de langage.
  * [jpythonfold.vim][2] : ajoute automatiquement des replis pour les classes / méthodes / fonctions de pythons.
  * [pyflakes.vim][3] : vérification au fil de l&rsquo;eau de la syntaxe python. `pyflakes` agit à la manière d&rsquo;un correcteur orthographique et souligne en rouge les lignes de codes incorrectes, les variables inutilisées ou non assignées &#8230;

### Installation

Dans un premier temps on installe `comments.vim` et `jpythonfold.vim`. S&rsquo;ils n&rsquo;existent pas créer les dossiers `.vim` dans votre dossier utilisateur et un sous dossier `plugin`

```sh
mkdir -pv $HOME/.vim/plugin
```

Copier simplement les fichiers `comments.vim` et `jpythonfold.vim` dans le dossier `.vim/plugin`.

Pour jpythonfold, il est pratique de ne l&rsquo;activer que lorsque le fichier est un fichier python. Pour cela l&rsquo;auteur propose de décommenter certaines lignes dans le fichier `jpythonfold.vim`. C&rsquo;est aux alentours de la ligne 40. Chez moi ça n&rsquo;a pas fonctionné. En m&rsquo;inspirant de ce qui est fait dans `comments.vim`, je vous propose de mettre ces lignes à la suite de celles proposées en laissant celles qui étaient proposées par l&rsquo;auteur commentées.

```vim
let file_name = buffer_name("%")
if file_name =~ '\.py$'
else
    finish
endif
```

C&rsquo;est assez simple à comprendre, si le nom de votre fichier se termine par `.py` alors le plugin est activé.

Pour installer `pyflakes`, la procédure est expliquée sur la page du plugin. S&rsquo;ils n&rsquo;existent pas créer les dossiers `ftplugin` et `python` respectivement dans les dossiers `$HOME/.vim` et `ftplugin` :

```sh
mkdir -pv $HOME/.vim/ftplugin/python
```

Après avoir téléchargé l&rsquo;archive de pyflakes, extraire le contenu dans `$HOME/.vim/ftplugin/python`. Il faut ensuite activer les ftplugin (pour File Type Plugin) qui assure le chargement du plugins en fonction du fichier considéré. S&rsquo;il n&rsquo;existe pas, créer le fichier `.vimrc` dans votre dossier utilisateur et ajouter la ligne suivante à l&rsquo;intérieur :

```vimrc
filetype plugin indent on
```

### Configuration supplémentaires de vi

Le fichier `.vimrc` permet de configurer (g)vim à votre goût. Voici quelques options spécifiques à la programmation qui peuvent être intéressantes :

```vimrc
syntax on               " coloration syntaxique
set showmatch        " affiche les paires de parenthèses

" pour l'indentation
set autoindent		" copy indent from the current line to the new line
set smartindent		" automatically indent a line depending on previous line
set expandtab           " remplace les tab par des espaces
set shiftwidth=4        " indentation = 4 espaces

" encodage
set encoding=utf-8

" une abreviation
ab pcom # * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
```

Pour plus d&rsquo;options de configurations de (g)vim, vous pouvez voir [cet article][4].

Bonne programmation !

 [1]: http://www.vim.org/scripts/script.php?script_id=1528
 [2]: http://www.vim.org/scripts/script.php?script_id=2527
 [3]: http://www.vim.org/scripts/script.php?script_id=2441
 [4]: http://gvallver.perso.univ-pau.fr/?p=283
