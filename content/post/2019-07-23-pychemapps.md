---
title: PyChemApps
subtitle: Web applications about chemical physics
author: Germain
type: post
toc: false
date: 2019-07-23
featured_image: /img/pychemapps.png
tags:
  - python
  - visualization
  - physical-chemistry
---

I finally start the [pychemapps](https://pychemapps.univ-pau.fr/) sever ! (thank you Joris Caravati)

![pychemapps landing page](/img/pychemapps.png)

This server is provided by the [University of Pau and Pays Adour](https://www.univ-pau.fr)
and will host web applications about chemistry, physics and physical-chemistry.
At the moment, the applications mainly rely on two technologies which are
[Django](https://www.djangoproject.com/) and [Plotly/Dash](https://plot.ly/dash/).

[Plotly/Dash](https://dash.plot.ly) is a Python Framework for building web
applications. It relies on Flask, to serve the application, React.js for the
interactivity and [Plotly](https://plot.ly/) to produce plots. Dash is really
simple to implement and is an efficient way to draw a quick web interface around
a python module and distribute the application to users which are not developers.

For the development of more specific applications about chemistry, physical-chemistry
and/or biology there are two dash modules that provide specific components.

* [dash-bio](https://github.com/plotly/dash-bio): is related to biology and chemistry.
It provides high-level charts or diagrams and 3D visualization tools.
* [dash-daq](https://dash.plot.ly/dash-daq): provides a set of controls to manage the
interactivity of your application (thermometer, gauge, button ...).
