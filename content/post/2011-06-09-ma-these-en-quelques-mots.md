---
title: Ma thèse en quelques mots
subtitle: Étude théorique de processus photophysiques dans des protéines fluorescentes
author: Germain
type: post
toc: false
date: 2011-06-09
featured_image: /img/CFP.png
tags:
  - physical-chemistry
  - sciences
---


[Le manuscrit de thèse est disponible sur Thèse En Ligne (TEL)](https://tel.archives-ouvertes.fr/tel-00431879)

---

![Photo de GFP, Tsien Lab](/img/FPS.jpg)
[*Image extraite du site du Tsien Lab*](http://www.tsienlab.ucsd.edu/)

Cette thèse présente une étude théorique de protéines de la famille de la _Green
Fluorescent Protein_, GFP. Ces protéines permettent grâce à leur propriétés de
fluorescence d’explorer un nombre croissant de processus biologiques in vivo. Les
approches numériques, complémentaires aux études expérimentales, peuvent apporter une
compréhension microscopique des processus mis en jeu et contribuer à
l'interprétation des propriétés photophysiques de ces protéines.

La première partie de ma thèse présente l'étude par simulation moléculaire de
l'effet du changement de pH sur la structure de la Cerulean et sur son spectre
d'absorption. Ces calculs nous ont permis d'établir que le décalage du spectre
d'absorption, observé expérimentalement en fonction du pH, est dû à une
isomérisation du chromophore liée au changement de l'orientation de la chaîne
latérale d'un acide aminé proche.

![CFP](/img/CFP.png)

La deuxième partie de la thèse aborde l'étude d'un mécanisme de
désactivation de la fluorescence dans la GFP. Nous avons proposé une approche, combinant
des simulations de dynamique moléculaire biaisée (*Umbrella Sampling*) et de dynamique brownienne, afin de
déterminer la cinétique d'un mécanisme de désactivation de la fluorescence lié à une
torsion du chromophore. Nous avons pu obtenir des distributions de temps de
n<sup>ème</sup> passage aux géométries critiques et en déduire des informations
quantitatives sur le déclin de fluorescence.

Les outils développés et leurs développements permettront de progresser dans la
compréhension de la relation entre l'isomérisation du chromophore, le pH et le
déclin de la fluorescence qui sont étroitement liés dans les protéines fluorescentes.

Ces travaux ont été réalisés en utilisant différentes méthodes de simulations numériques :
Simulations de dynamiques moléculaires (AMBER), calculs de chimie quantique (Gaussian),
codes personnels (Fortran 77 et ou Fortran 90).

#### Publications

Voici les publications associées à mes travaux de thèses :

* Demachy, I.; Ridard, J.; Laguitton-Pasquier, H.; Durnerin, E.; Vallverdu, G.; Archirel,
  P.; Lévy, B. Cyan Fluorescent Protein: Molecular Dynamics, Simulations, and Electronic
  Absorption Spectrum. *J. Phys. Chem. B* **2005**, 109 (50), 24121–24133.
  https://doi.org/10.1021/jp054656w.d
* Villoing, A.; Ridhoir, M.; Cinquin, B.; Erard, M.; Alvarez, L.; Vallverdu, G.; Pernot,
  P.; Grailhe, R.; Mérola, F.; Pasquier, H. Complex Fluorescence of the Cyan Fluorescent
  Protein: Comparisons with the H148D Variant and Consequences for Quantitative Cell
  Imaging. *Biochemistry* **2008**, 47 (47), 12483–12492. https://doi.org/10.1021/bi801400d.d
* Vallverdu, G.; Demachy, I.; Ridard, J.; Lévy, B. Using Biased Molecular Dynamics and
  Brownian Dynamics in the Study of Fluorescent Proteins. *Journal of Molecular Structure:
  THEOCHEM* **2009**, 898 (1–3), 73–81. https://doi.org/10.1016/j.theochem.2008.07.012.d
* Vallverdu, G.; Demachy, I.; Mérola, F.; Pasquier, H.; Ridard, J.; Lévy, B. Relation
  between PH, Structure, and Absorption Spectrum of Cerulean: A Study by Molecular
  Dynamics and TD DFT Calculations. *Proteins: Structure, Function, and Bioinformatics*
  **2010**, 78 (4), 1040–1054. https://doi.org/10.1002/prot.22628.d
* Jonasson, G.; Teuler, J.-M.; Vallverdu, G.; Mérola, F.; Ridard, J.; Lévy, B.; Demachy,
  I. Excited State Dynamics of the Green Fluorescent Protein on the Nanosecond Time Scale.
  *J. Chem. Theory Comput.* **2011**, 7 (6), 1990–1997. https://doi.org/10.1021/ct200150r.d

