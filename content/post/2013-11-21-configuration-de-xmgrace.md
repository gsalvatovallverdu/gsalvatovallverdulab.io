---
title: "Configuration de (xm)grace"
author: Germain
type: post
date: 2013-11-21
featured_image: /img/grace.png
tags:
  - xmgrace
  - visualization
---

Grace et plus précisément sa version graphique xmgrace (anciennement xmgr) est un outil permettant de concevoir des graphiques en deux dimensions (XY, polaire, logarithmique, boxplot &#8230;) ainsi que faire un peu d&rsquo;analyse numérique (régression, interpolation, intégration &#8230;) et réaliser quelques traitements sur les données. Les graphiques obtenus sont de haute qualité et peuvent être exportés sous divers formats vectoriels (eps, ps, svg) ou non (png, jpg &#8230;). Le paquet est disponible sous ubuntu ([grace](https://launchpad.net/ubuntu/+source/grace)), fedora et la plupart des distributions linux. Sous MacOS X, il est disponible via [Hombrew](https://formulae.brew.sh/formula/grace).

site web : [plasma-gate.weizmann.ac.il/Grace][1].

## Accents et séparateur des décimaux

Par défaut, les accents ne sont pas pris en charge. Il faut passer un paramètre de langue au lancement de xmgrace pour pouvoir les utiliser. Sur la ligne de commande, lancer xmgrace de la façon suivante :

```sh
[user@machine] > LANG=C xmgrace
```

ou

```sh
[user@machine] > LANG=fr_FR xmgrace
```

Suivant votre distribution, le paramètre `LANG=fr_Fr` activera les accents et conservera un séparateur des décimaux avec une virgule, tandis que `LANG=C` activera les accents et changera le séparateur de décimaux en un point. Il est possible d’enregistrer ces paramètres dans un alias pour simplifier l&rsquo;utilisation. Pour cela, ajouter la ligne suivante dans votre fichier `.bashrc` ou `.profile` :

```sh
alias xm="LANG=fr_FR xmgrace -geometry 1150x900"
```

L&rsquo;option `geometry` permet quand à elle de faire en sorte que la fenêtre de xmgrace soit suffisamment grande pour voir la totalité du graphique.

## Template

Pour éviter d&rsquo;avoir à saisir systématiquement les mêmes paramètres (épaisseur des bordures, taille du texte &#8230;) il est possible de créer un template qui sera chargé automatiquement à l&rsquo;ouverture du programme. Pour ce faire, créer un graphique vide avec toutes les options qui vous intéressent et sauvegarder le sous le nom `Default.agr`. Placer ensuite ce fichier dans :

```sh
~/.grace/templates/
```

Si les dossiers n&rsquo;existent pas, il faut les créer.

Ce fichier `Default.agr` permet également de définir de nouvelles couleurs pour étoffer un peu la palette proposée par défaut qui est un peu pauvre. Voici, par exemple, les lignes à insérer pour disposer d&rsquo;une partie des couleurs de bases plus celles de la palette [tango][2]. Ces lignes sont à placer **à la place** de celles définissant les couleurs dans le fichier `Default.agr` (commençant par `@map`). Pour ce faire utiliser un éditeur de texte quelconque tel que vim, gedit, emacs ... Attention aux numéros des couleurs qui doivent être uniques et dans l'ordre.

```
@map color  0 to (255, 255, 255), "white"
@map color  1 to (  0,   0,   0), "black"
@map color  2 to (255,   0,   0), "red"
@map color  3 to (  0, 255,   0), "green"
@map color  4 to (  0,   0, 255), "blue"
@map color  5 to (148,   0, 211), "violet"
@map color  6 to (255, 165,   0), "orange"
@map color  7 to (  0, 255, 255), "cyan"
@map color  8 to (255,   0, 255), "magenta"
@map color  9 to (114,  33, 188), "indigo"
@map color 10 to (103,   7,  72), "maroon"
@map color 11 to ( 64, 224, 208), "turquoise"
@map color 12 to (  0, 139,   0), "green4"
@map color 13 to (252, 233,  79), "Butter 1"
@map color 14 to (237, 212,   0), "Butter 2"
@map color 15 to (196, 160,   0), "Butter 3"
@map color 16 to (138, 226,  52), "Chameleon 1"
@map color 17 to (115, 210,  22), "Chameleon 2"
@map color 18 to ( 78, 154,   6), "Chameleon 3"
@map color 19 to (252, 175,  62), "Orange 1"
@map color 20 to (245, 121,   0), "Orange 2"
@map color 21 to (206,  92,   0), "Orange 3"
@map color 22 to (114, 159, 207), "Sky Blue 1"
@map color 23 to ( 52, 101, 164), "Sky Blue 2"
@map color 24 to ( 32,  74, 135), "Sky Blue 3"
@map color 25 to (173, 127, 168), "Plum 1"
@map color 26 to (117,  80, 123), "Plum 2"
@map color 27 to ( 92,  53, 102), "Plum 3"
@map color 28 to (233, 185, 110), "Chocolate 1"
@map color 29 to (193, 125,  17), "Chocolate 2"
@map color 30 to (143,  89,   2), "Chocolate 3"
@map color 31 to (239,  41,  41), "Scarlet Red 1"
@map color 32 to (204,   0,   0), "Scarlet Red 2"
@map color 33 to (164,   0,   0), "Scarlet Red 3"
@map color 34 to (238, 238, 236), "Aluminium 1"
@map color 35 to (211, 215, 207), "Aluminium 2"
@map color 36 to (186, 189, 182), "Aluminium 3"
@map color 37 to (136, 138, 133), "Aluminium 4"
@map color 38 to ( 85,  87,  83), "Aluminium 5"
@map color 39 to ( 46,  52,  54), "Aluminium 6"
```

Lorsque vous choisissez une couleur, vous avez maintenant une palette plus importante :

![New colors in xmgrace](/img/grace_color.png)

Voici le fichier `Default.agr` que j'utilise. Il peut vous servir de point de départ :

* [Fichier `Default.agr`](/docs/Default.agr)

## Exportation par défaut

Dans le dossier :

```
~/.grace/
```

Créer un fichier `gracerc.user` contenant les lignes suivantes pour que le format de sortie par défaut soit `.eps` :

```
# gracerc
#
# set esp default print format
HARDCOPY DEVICE "EPS"
```

Il sera alors inutile de passer par le menu &laquo;&nbsp;print setup&nbsp;&raquo; avant d&rsquo;exporter un graphique. Changer le paramètre `"EPS"` par le format de sortie qui vous convient.

 [1]: http://plasma-gate.weizmann.ac.il/Grace
 [2]: http://tango.freedesktop.org/Tango_Icon_Theme_Guidelines
