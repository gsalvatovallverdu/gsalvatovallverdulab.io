---
title: Treemaps plot using matplotlib and python
author: Germain
type: post
date: 2016-07-18
featured_image: /img/capp_treemaps.png
toc: false
tags:
  - matplotlib
  - treemap
---
Here is an example of a `treemap` plot using `python`, `matplotlib` and the [`squarify`](https://github.com/laserson/squarify) module.

The data comes from the open data of the [Communauté d&rsquo;Agglomération Pau-Pyrénées](https://opendata.agglo-pau.fr/index.php/fiche?idQ=27).

The idea is the following :

  * The area of each square is the surface area of the towns
  * The color scale is over the population in 2011

#### Source code :

<script src="https://gist.github.com/gVallverdu/0b446d0061a785c808dbe79262a37eea.js"></script>


#### Output

Because of the large population of Pau against other towns
in the *communauté des communes*, I draw a white square
background and I did not include it in the color scale.
Nevertheless, the area of the rectangle is consistent with
the area of Pau.

The plot was produced using the following packages and python versions:

```sh
python     :  3.7.6
pandas     :  1.0.1
matplotlib :  3.1.3
squarify   :  0.4.3
```

![Treemap of CAPP in 2011](/img/capp_treemaps.png)
