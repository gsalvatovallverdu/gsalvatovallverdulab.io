---
title: À propos de moi
subtitle: Physico-chimie et simulation numérique
toc: false
comments: false
---

#### Germain Salvato Vallverdu

<img style="float: left; margin: 0 10px 5px 0; display: inline; vertical-align:middle" src="/img/gvallver.jpg" alt="photo germain" width="25%"/>

Bonjour et bienvenu sur mon site !

L’objectif de ce site est de partager des conseils, astuces ou tutoriaux en lien avec les technologies numériques, la programmation en général, l’écosystème python en particulier et la chimie-physique. La plupart des articles répondent à des questions que je me suis posées ou des besoins que j’ai eu à un moment donné. Les mettre sur ce site me permet de les diffuser/partager et de les retrouver quand j’en ai besoin.

Je suis déjà content que vous soyez arrivé jusqu’ici et j’espère que vous avez trouvé ce que vous cherchiez. N'hésitez pas à me contacter si vous avez des questions et/ou suggestions !

Pour plus d'information sur mes activités de recherche vous pouvez consulter
ma page sur le site de l'université : [<i class="fas fa-user-graduate"></i> gvallver.perso.univ-pau.fr](http://gvallver.perso.univ-pau.fr/).
